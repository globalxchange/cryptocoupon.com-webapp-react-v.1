import Routes from "./Routes";
import "bootstrap-scss/bootstrap.scss";
import "./static/scss/master.scss";
import "@teamforce/fullpage-search/dist/index.css";
import { AgencyProvider } from "./context/Context";

function CryptoCouponsApp() {
  return (
    <AgencyProvider>
      <Routes />
    </AgencyProvider>
  );
}

export default CryptoCouponsApp;
