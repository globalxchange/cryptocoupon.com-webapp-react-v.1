import React, { Component, createContext } from "react";

import axios from "axios";


export const Agency = createContext();
export class AgencyProvider extends Component {
  
  state = {
banner:false
  };
functionbanner=()=>{
  this.setState({
    banner:true
  })
}

  render() {
    return (
      <Agency.Provider
        value={{
          ...this.state,
          functionbanner:this.functionbanner
        }}
      >
        {/* <ContextDevTool context={Agency} id="uniqContextId" displayName="Context Display Name" /> */}
        {this.props.children}
      </Agency.Provider>
    );
  }
}
