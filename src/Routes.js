import React from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import ConverterPage from "./pages/ConverterPage";
import HomePage from "./components/HomePage/NavbarHome";
import Login from "./components/HomePage/LogoinPage";
function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/login" component={Login} />
      </Switch>
    </BrowserRouter>
  );
}

export default Routes;
