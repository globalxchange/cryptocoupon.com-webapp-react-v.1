import React,{useEffect,useState} from 'react'
import Carousel from "react-multi-carousel";
import './Popular.scss'
import "react-multi-carousel/lib/styles.css";
import {Homes} from './HomeApi'
import cashback from '../../static/images/cashback.png'




const list=[
  {
    name:"TODAY'S BEST OFFERS"
  },
    {
      name:"COUPONDUNIA EXCLUSIVES"
  },{
    name:"PEOPLE ARE CURRENTLY USING"
  }
]
const responsive = {
  desktop: {
    breakpoint: { max: 10000, min: 1024 },
    items: 9,
    partialVisibilityGutter: 40,
    slidesToSlide: 9
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,

    slidesToSlide: 2
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 2,

    slidesToSlide: 2,
  }
};
const images = [

  "https://images.unsplash.com/photo-1550133730-695473e544be?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550167164-1b67c2be3973?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550338861-b7cfeaf8ffd8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  
  "https://images.unsplash.com/photo-1550353175-a3611868086b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550330039-a54e15ed9d33?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549737328-8b9f3252b927?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549833284-6a7df91c1f65?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549985908-597a09ef0a7c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550064824-8f993041ffd3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
];

export default function MainCarsole() {
   const [prev,setprev]=useState(0)
   const [next,setnext]=useState(8)
   const [name,setname]=useState("TODAY'S BEST OFFERS")
   const nextFucntion=(e)=>{
    if(next<16){
    setprev(next)
    setnext(next + e)
    }   
   }

   const prevFucntion=(e)=>{
if(prev>0){
    setprev(prev - e)
    setnext(next -e)
}

   }
    useEffect(() => {
        let multiples=[];
        let finial=[]
        for (var i = 0; i <= Homes.length; i++) {
            if (i % 9 === 0) { // divide by the number
              multiples.push(i); // add the current multiple found to the multiples array
            }
            else{
               
            }
          }
          for (var i = 1; i < multiples.length; i++) {
            if (i % 1 === 0) { // divide by the number
                finial.push(i); // add the current multiple found to the multiples array
            }
            else{
               
            }
          }
      
        
        return () => {
        }
    }, [])

    console.log("prev", prev)
    console.log("next", next)
    return (
        <>
        <div className="SotriesMain">
<div class="block-heading">
POPULAR OFFERS

</div>

<div className="labelsection">

  {
    list.map(item=>{
      return(
        <label htmlFor="" style={name==item.name?{background: "#0000ff0a"}:{}} onClick={()=>setname(item.name)}>{item.name}</label>
      )
    })
  }
    {/* <label htmlFor="" style={{background: "#0000ff0a"}}>TODAY'S BEST OFFERS</label>
   
    <label htmlFor="">PEOPLE ARE CURRENTLY USING</label> */}
</div>
        <div className="Poplularsection"> 

        <div className="customCarosule">
        
        {Homes.slice(prev,next).map(item => {
          return (
             <div className="cards">
         <p>40% OFF</p>
               
                     <img src={item.img} alt=""/>
              
                 <div className="cardsecond">
                 <h3> Flat Rs. 150 Off on Your Orders </h3>
       <div className="cpnbtn">
       ATO 
       <div class="p1"></div>
       <div class="p2">
           <div class="t1">
               </div>
               <div class="t1">
                   <div class="t2">
                       </div>
                       </div>
                
                       </div>
                       <span>Show Coupon Code</span>
       </div>
        
                 </div>
                
             </div>
        
          );
        })}
   
   {/* {
      next>8?
      <div className="cards">
      <p>5345</p>
  </div>
  :null
   } */}
   {/* {
       prev>0?
       <label className="prev" onClick={()=>prevFucntion(8)}>{`<`}</label>
       :
       null
   }

   {
       next < 18?
       <label className="next" onClick={()=>nextFucntion(8)}>{`>`}</label>
       :
       ""
   } */}
 
     
   
          </div>

        </div>
 
        </div>

</>

    )
}
