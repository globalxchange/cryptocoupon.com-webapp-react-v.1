import React,{useEffect,useState} from 'react'
import Carousel from "react-multi-carousel";
import './HomePage.scss'
import "react-multi-carousel/lib/styles.css";
import {Homes} from './HomeApi'
import cashback from '../../static/images/cashback.png'
const responsive = {
  desktop: {
    breakpoint: { max: 10000, min: 1024 },
    items: 9,
    partialVisibilityGutter: 40,
    slidesToSlide: 9
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,

    slidesToSlide: 2
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 2,

    slidesToSlide: 2,
  }
};
const images = [

  "https://images.unsplash.com/photo-1550133730-695473e544be?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550167164-1b67c2be3973?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550338861-b7cfeaf8ffd8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  
  "https://images.unsplash.com/photo-1550353175-a3611868086b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550330039-a54e15ed9d33?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549737328-8b9f3252b927?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549833284-6a7df91c1f65?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549985908-597a09ef0a7c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550064824-8f993041ffd3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
];

export default function MainCarsole() {
   const [prev,setprev]=useState(0)
   const [next,setnext]=useState(9)
   const nextFucntion=(e)=>{
    if(next<18){
    setprev(next)
    setnext(next + e)
    }   
   }

   const prevFucntion=(e)=>{
if(prev>0){
    setprev(prev - e)
    setnext(next -e)
}

   }
    useEffect(() => {
        let multiples=[];
        let finial=[]
        for (var i = 0; i <= Homes.length; i++) {
            if (i % 9 === 0) { // divide by the number
              multiples.push(i); // add the current multiple found to the multiples array
            }
            else{
               
            }
          }
          for (var i = 1; i < multiples.length; i++) {
            if (i % 1 === 0) { // divide by the number
                finial.push(i); // add the current multiple found to the multiples array
            }
            else{
               
            }
          }
      
        
        return () => {
        }
    }, [])

    console.log("prev", prev)
    console.log("next", next)
    return (
        <>
        <div className="SotriesMain">
<div class="block-heading">
TOP STORES

</div>
        <div className="topStorysection"> 

        <div className="featureStore">
<div className="fb-title">

<h1>FEATURED STORE</h1>

</div>

<div className="imgasection">
    <img src="https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/stores/web/6c4d5fc5e5d0d5e3df7ea5bf816ac186/tatacliq-logo-small.jpg?833495" alt=""/>

</div>
<h3>PharmEasy</h3>
    <p>Flat Rs. 250 CD Cashback</p>
    <label htmlFor="">Activate Cashback</label>
        </div>
        <div className="customCarosule">
        
        {Homes.slice(prev,next).map(item => {
          return (
             <div className="cards">

                 <div className="cardfirst">
                     <img src={item.img} alt=""/>
                 </div>
                 <div className="cardsecond">
                 <h3>{item.name}</h3>
                 <p>{item.price}% CD Voucher Rewards</p>
                 <h5>{item.offer} Offers</h5>
                 </div>
                
             </div>
        
          );
        })}
   
   {/* {
      next>8?
      <div className="cards">
      <p>5345</p>
  </div>
  :null
   } */}
   {/* {
       prev>0?
       <label className="prev" onClick={()=>prevFucntion(9)}>{`<`}</label>
       :
       null
   }

   {
       next < 18?
       <label className="next" onClick={()=>nextFucntion(9)}>{`>`}</label>
       :
       ""
   }
  */}
     
   
          </div>

        </div>
 
        </div>

</>

    )
}
