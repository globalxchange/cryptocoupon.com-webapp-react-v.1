import React,{useEffect} from 'react'
import Carousel from "react-multi-carousel";
import './Cashback.scss'
import "react-multi-carousel/lib/styles.css";
import {Homes} from './HomeApi'
import cashback from '../../static/images/cashback.png'
import TopStories from './TopStories'
const responsive = {
  desktop: {
    breakpoint: { max: 10000, min: 1024 },
    items: 4,
    partialVisibilityGutter: 40,
    slidesToSlide: 4
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,

    slidesToSlide: 2
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 2,

    slidesToSlide: 2,
  }
};
const images = [
"https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/686b05560a692e65d0176eda6e7cd6c9/banner_home-290x580.jpg?335174",
  "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/be77deea2adf00e2beec14a316bed6dc/banner_home-290x580.jpg?526085",
  "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/066e4334c4f3e7879a6606040b498282/banner_home-290x580.jpg?609092",
  "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/3d8fcab9d683b8c86f3cb3fa3ab89563/banner_home-290x580.jpg?672518",
  "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/c96091c368bcb58249891929c6222e24/banner_home-290x580.jpg?467538",
  "https://images.unsplash.com/photo-1550353175-a3611868086b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",

 "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/d368acbff0cfca8c7521d9a8575d6229/banner_home-290x580.jpg?331310",
  "https://images.unsplash.com/photo-1550330039-a54e15ed9d33?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549737328-8b9f3252b927?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549833284-6a7df91c1f65?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549985908-597a09ef0a7c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550064824-8f993041ffd3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
];

export default function MainCarsole() {
   

    return (
        <>
        <div className="carsoulesectioncashback ">
        <div class="block-heading">
        CASHBACK INCREASED

</div>
                <Carousel
                swipeable={false}
                draggable={false}
  ssr
  infinite={false}
  autoPlay={false}
  autoPlaySpeed={1000}
  keyBoardControl={false}
  customTransition="all .8"
  transitionDuration={500}
  containerClass="carousel-container"
  responsive={responsive}
  minimumTouchDrag={80}
  deviceType="desktop"
  renderButtonGroupOutside={false}
  dotListClass="custom-dot-list-style"

    >
      {Homes.map(item => {
        return (
            <div className="cashbakcard">
            <img src={item.img} alt=""/> 
            <h5>{item.name}</h5>
        <h2>Upto {item.offer} CD Rewards</h2>
        <label >Activite Rewards</label>
        </div>
      
        );
      })}
    </Carousel>


        </div>

</>
    )
}
