
import React,{useEffect,useState} from 'react'
import search from '../../static/images/search.png'
import M  from './MainCarsole'
import couple from '../../static/images/couple.png'
import coupon from '../../static/images/coupon.png'
import brand from '../../static/images/brand.png'
import catoriges from '../../static/images/catoriges.png'
import promo from '../../static/images/promo.png'
import scrolllogo from '../../static/images/scrolllogo.png'
import logo from '../../static/images/logo.png'
import './NabvarHome.scss'

import india from '../../static/images/india.png'
import apple from '../../static/images/apple.png'
import applelogo from '../../static/images/applelogo.png'
import today from '../../static/images/today.png'

import bb from '../../static/images/bb.png'
import { useHistory, useParams } from "react-router-dom";


const sapple=[
  {
    name:"IPhone X",
    color:"#E62129"
  },
 
  {
    name:"IPhone X",
    color:"#FFA001"
  },
  {
    name:"IPhone X",
    color:"#FFDA2D"
  },
  {
    name:"IPhone X",
    color:"#E62129"
  },
  {
    name:"IPhone X",
    color:"#E62129"
  },
]

const dropdownnname=[
    {
        name:"Categories",
    },
    {
        name:"Top Products",
    },{
        name:"Top Stores",
    },{
        name:"Best Offers",
    },
    {
        name:"Collections",
    },
    {
        name:"Refer & Earn",
    }
]

const modeldata=[
    {
      img:coupon,
      name:"Copouns"
    },
    {
      img:brand,
      name:"Brands"
    },{
      img:catoriges,
      name:"Categories",
    },
    {
      img:promo,
      name:"Promotions"
    },
  ]
export default function NavbarHome() {
  const history = useHistory();
    let listener = null
    const [scrollState, setScrollState] = useState("sub")
    const [navimage,setnavimage]=useState(coupon)
   const [navname,setnavname]=useState('Copouns')
   const  [param,setparam]=useState('Find A Copoun....')
   const [textvalue,settextvalue]=useState('')
const [show,setshow]=useState(false)
const [listserch,setlistserch]=useState(sapple)
const [showsearch,setshowsearch]=useState(false)
const namechange=(e)=>{
    let s=e.target.value
    settextvalue(s)
if(e.target.value.length>0)
{
if(navname=="Copouns"||navname=="Brands"||navname=="Categories")
{
    setshowsearch(true)
   
    // let n1= sapple.filter((user)=>{
    //     return (user.name).toUpperCase().includes((s).toUpperCase());
        
    //    })
    // setlistserch(n1)
}
}
else{
    setshowsearch(false)
}
}
   const nvabaritem=(e)=>{
    setnavimage(e.img)

    settextvalue('')
    setshow(false)
    if(e.name=="Copouns")
    {
        setparam("Find A Copoun....") 
        setnavname(e.name)
    }
    else if(e.name=="Brands")
    {
        setparam("Find A Brand....") 
        setnavname(e.name)
      }
    
    else if(e.name=="Promotions")
    {
        setparam("Enter Promo Code....") 
    
        setnavname("Promo") 
       }
    
    else if(e.name=="Categories")
    {
        setparam("Search Product Categories...") 
        setnavname(e.name)
      }
    

   }
    useEffect(() => {
      listener = document.addEventListener("scroll", e => {
        var scrolled = document.scrollingElement.scrollTop
        if (scrolled >= 100) {
       
            setScrollState("scrollsub")
        } else {
          if (scrollState !== "top") {
            setScrollState("sub")
          }
        }
      })
      return () => {
        document.removeEventListener("scroll", listener)
      }
    }, [scrollState])
    return (
        <>
    <div>

  
<div className="header-wrapper" onClick={()=>  setshowsearch(false)}>
<div className="super-header ">
    <div className="content-wrapper ">
<div className="navbar-header ">
    <div className="brand" onClick={()=>history.push(`/`)}>
<div className={scrollState}>
    {
        scrollState=="sub"?
        <img src={logo} draggable="false" alt=""/>
        :
        <img src={scrolllogo} draggable="false" alt=""/>
        
    }

</div>
    </div>
    <div className="top">
    {
        dropdownnname.map(item=>{
            return(
                <p>{item.name}</p>
            )
        })
    }
    </div>

</div>
    </div>

</div>
<div className="sub-header">
    <div className="d-flex " style={{height:"100%",background:"white"}}>
    <div className="inputsection">
    <input type="text" value={textvalue} placeholder={param} onChange={namechange}/>
    </div>
<div className="logosection" onClick={()=>{setshow(true);setshowsearch(false)}}>
    <img src={navimage} alt=""/>
    <p>{navname}</p>
</div>
</div>
</div>
</div>
</div>

<M
modeldata={modeldata}
navname={navname}
nvabaritem={nvabaritem}
show={show}
listserch={listserch}
showsearch={showsearch}
setshowsearch={setshowsearch}
setshow={setshow}
/>


        </>
    )
}
